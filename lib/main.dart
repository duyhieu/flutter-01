import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/weather_bloc.dart';
import 'data/weather_repository.dart';
import 'pages/weather_search_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather App',
      //cung cấp bloc
      home: BlocProvider(
        // built Bloc để các child của nó có thể sử dụng  , nó tự động sử lý để đóng kết nối
        // cung cấp 1 thể hiện WeatherBloc duy nhất cho các child của nó
        builder: (context) => WeatherBloc(FakeWeatherRepository()),
        child: WeatherSearchPage(),
      ),
    );
  }
}
